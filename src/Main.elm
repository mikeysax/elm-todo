module Main exposing (..)

import Browser
import Html exposing (Attribute, Html, button, div, form, h1, input, text)
import Html.Attributes exposing (for, id, placeholder, style, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import List exposing (..)



-- MAIN


main =
    Browser.sandbox
        { init = init
        , update = update
        , view = view
        }



-- MODEL


type alias Model =
    { text : String
    , editedText : String
    , todos : List String
    , editable : Int
    }


init : Model
init =
    { text = ""
    , editedText = ""
    , todos = []
    , editable = 100
    }



-- UPDATE


type Msg
    = InputNewTodoText String
    | SubmitNewTodo
    | DeleteTodo Int
    | EditTodo Int String
    | SubmitUpdateTodo Int
    | InputEditTodoText String
    | CancelUpdate


addTodo : List String -> String -> List String
addTodo todos newTodo =
    List.append todos [ newTodo ]


removeTodo : List String -> Int -> List String
removeTodo todos deletedIndex =
    todos
        |> List.indexedMap (\index text -> ( index, text ))
        |> List.filter (\( index, _ ) -> index /= deletedIndex)
        |> List.map Tuple.second


modifyTodo : Model -> Int -> List String
modifyTodo model modifyIndex =
    model.todos
        |> List.indexedMap
            (\index text ->
                if index == modifyIndex then
                    model.editedText

                else
                    text
            )


setEditableToNoTodo : List String -> Int
setEditableToNoTodo todos =
    List.length todos + 100


update : Msg -> Model -> Model
update msg model =
    case msg of
        InputNewTodoText value ->
            { model
                | text = value
            }

        SubmitNewTodo ->
            { model
                | text = ""
                , todos = addTodo model.todos model.text
                , editable = setEditableToNoTodo model.todos
            }

        DeleteTodo index ->
            { model
                | todos = removeTodo model.todos index
                , editable = setEditableToNoTodo model.todos
            }

        EditTodo index text ->
            { model
                | editable = index
                , editedText = text
            }

        InputEditTodoText value ->
            { model
                | editedText = value
            }

        SubmitUpdateTodo index ->
            { model
                | todos = modifyTodo model index
                , editedText = ""
                , editable = setEditableToNoTodo model.todos
            }

        CancelUpdate ->
            { model
                | editedText = ""
                , editable = setEditableToNoTodo model.todos
            }



-- VIEW STYLES


todoContainerStyles : List (Attribute Msg)
todoContainerStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "flex-direction" "column"
    , style "padding" "30px"
    , style "box-sizing" "border-box"
    ]


todoFormStyles : List (Attribute Msg)
todoFormStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "flex-direction" "row"
    , style "width" "100%"
    , style "padding" "20px"
    , style "box-sizing" "border-box"
    , style "border" "1px solid #333"
    , style "border-radius" "3px"
    , style "box-shadow" "0 2px 5px #33333333"
    , style "background-color" "#eee"
    ]


todoListStyles : List (Attribute Msg)
todoListStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "flex-direction" "column"
    , style "width" "100%"
    , style "box-sizing" "border-box"
    ]


todoBodyStyles : List (Attribute Msg)
todoBodyStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "border" "1px solid #333"
    , style "border-radius" "3px"
    , style "padding" "20px"
    , style "margin" "10px"
    , style "box-shadow" "0 2px 5px #33333333"
    , style "box-sizing" "border-box"
    , style "width" "100%"
    , style "background-color" "#eee"
    ]


todoButtonContainerStyles : List (Attribute Msg)
todoButtonContainerStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "flex-direction" "row"
    ]


todoButtonStyles : List (Attribute Msg)
todoButtonStyles =
    [ style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "center"
    , style "border" "1px solid black"
    , style "border-radius" "3px"
    , style "padding" "5px 10px"
    , style "margin" "0 10px"
    , style "margin-bottom" "2.5px"
    , style "font-size" "16px"
    , style "background-color" "#33839250"
    , style "box-shadow" "0 2px 5px #33333333"
    , style "box-sizing" "border-box"
    ]


todoTextStyles : List (Attribute Msg)
todoTextStyles =
    [ style "font-size" "16px"
    , style "margin" "10px"
    , style "width" "100%"
    ]


todoInputStyles : List (Attribute Msg)
todoInputStyles =
    [ style "display" "flex"
    , style "align-items" "flex-start"
    , style "justify-content" "flex-start"
    , style "box-sizing" "border-box"
    , style "width" "100%"
    , style "padding" "7px"
    , style "border-radius" "3px"
    , style "border" "1px solid #333"
    , style "box-shadow" "inset 0 2px 5px #33333333"
    ]



-- VIEW FUNCTIONS


renderTodoList : Model -> Html Msg
renderTodoList model =
    model.todos
        |> List.indexedMap (\index todo -> ( index, todo ))
        |> List.map
            (\( index, todo ) ->
                if index == model.editable then
                    editTodo index model.editedText

                else
                    renderTodo ( index, todo )
            )
        |> div ([] ++ todoListStyles)


renderTodo : ( Int, String ) -> Html Msg
renderTodo ( index, todo ) =
    div ([] ++ todoBodyStyles)
        [ div ([] ++ todoTextStyles) [ text todo ]
        , div ([] ++ todoButtonContainerStyles)
            [ button ([ onClick (EditTodo index todo) ] ++ todoButtonStyles) [ text "Edit" ]
            , button ([ onClick (DeleteTodo index) ] ++ todoButtonStyles) [ text "Delete" ]
            ]
        ]


editTodo : Int -> String -> Html Msg
editTodo index editedText =
    div ([] ++ todoBodyStyles)
        [ input ([ value editedText, onInput InputEditTodoText ] ++ todoInputStyles) []
        , div ([] ++ todoButtonContainerStyles)
            [ button ([ onClick (SubmitUpdateTodo index) ] ++ todoButtonStyles) [ text "Update" ]
            , button ([ onClick CancelUpdate ] ++ todoButtonStyles) [ text "Cancel" ]
            ]
        ]



-- VIEW


view : Model -> Html Msg
view model =
    div ([] ++ todoContainerStyles)
        [ h1 [] [ text "Elm Todo" ]
        , form ([ id "todo-form", onSubmit SubmitNewTodo ] ++ todoFormStyles)
            [ input ([ placeholder "Enter New Todo...", for "todo-form", value model.text, onInput InputNewTodoText ] ++ todoInputStyles) []
            , button ([] ++ todoButtonStyles) [ text "Submit" ]
            ]
        , renderTodoList model
        ]
